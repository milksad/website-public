# [milksad.info](https://milksad.info)

Web page source.

## Instructions

1. Install `make`.
2. Install `docker`
3. Build Docker container

```shell
$ make build
```

4. Serve site at `0.0.0.0:4000`:

```shell
$ make serve
```

### Publish Docker container to Registry

1. Use `docker login` with a permission token with write package scope
2. Tag container with repo name

```shell
docker tag milksad-distrust-co git.distrust.co/milksad/website
```

3. Push container to container registry

```sh
docker push git.distrust.co/milksad/website
```


### Generate Static Files

1. Output static files in `_site` directory:

```shell
$ make _site
```

### Cleanup

1. Remove all build artifacts:

```shell
$ make fullclean
```

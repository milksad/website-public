# flexible use with docker and podman
platform ?= docker
container_workdir = /home/builder/workdir/
# useful option: "--userns=keep-id"
# platform_extra_options


.PHONY: build
build:
	# Build Docker image
	$(platform) build -t milksad-website .

.PHONY: fullclean
fullclean: clean
	$(platform) rmi milksad-website-builder -f
	$(platform) image prune --filter label=stage=milksad-website-builder

.PHONY: clean
clean:
	rm -rf site_export

site_export: build
	rm -rf site_export
	mkdir -p site_export
	$(platform) run milksad-website tar c -C ${container_workdir}/_site . | tar x -C site_export

#.PHONY: serve
#serve: build
#	# Run Docker container with listener for current dir and port mapping
#	$(platform) run --rm -p 0.0.0.0:4000:80 -it milksad-website

upload: site_export
	rsync -av --delete site_export/ milksad-website:/var/www/html

.PHONY: build-dev
build-dev:
	# Build Docker image
	$(platform) build --target builder -t dev-milksad-website .

.PHONY: dev
dev: build-dev
	$(platform) run --rm --expose 4000 -p 127.0.0.1:4000:4000 --mount type=bind,source=${PWD},target=${container_workdir} $(platform_extra_options) -it dev-milksad-website jekyll serve -H 0.0.0.0

# do not depend on build-dev, and rely on everything being ready
dev-no-rebuild:
	$(platform) run --rm --expose 4000 -p 127.0.0.1:4000:4000 --mount type=bind,source=${PWD},target=${container_workdir} $(platform_extra_options) -it dev-milksad-website jekyll serve -H 0.0.0.0


.PHONY: dev-shell
dev-shell: build-dev
	$(platform) run --rm --expose 4000 -p 127.0.0.1:4000:4000 --mount type=bind,source=${PWD},target=${container_workdir} $(platform_extra_options) -it dev-milksad-website sh

module LinkBitcoin
    def BtcLinkTxUrlSliced(input)
       "[#{input.slice(0,8)}..#{input.slice(-8,8)}](https://mempool.space/tx/#{input})"
    end
    def BtcLinkAddressUrlFull(input)
        "[#{input}](https://mempool.space/address/#{input})"
     end
end

Liquid::Template.register_filter(LinkBitcoin)
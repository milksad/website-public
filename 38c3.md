---
title: /38c3
layout: home
permalink: /38c3/
---

# Milk Sad at 38C3
## Talk Info
We'll present a talk at the 38th Chaos Communication Congress (38C3) which takes place in Hamburg on 27–30 Dec 2024.
[https://fahrplan.events.ccc.de/congress/2024/fahrplan/talk/PEN9QU/](https://fahrplan.events.ccc.de/congress/2024/fahrplan/talk/PEN9QU/)

Talk starts **2024-12-30 11:00 CET** (_= 11am on "day 4"_) in hall `GLITCH`.
The talk will be in English and streamed live via [media.ccc.de](https://streaming.media.ccc.de/).

After the official short Q&A on-stage, we have an extended [Q&A workshop session](https://events.ccc.de/congress/2024/hub/en/event/qa-dude-wheres-my-crypto/) right after the talk (no registration required).

In case you're reading this before the talk takes place, feel free to ask for us at the [Church of Cryptography](https://events.ccc.de/congress/2024/hub/en/assembly/CoC/) assembly, where our team member and talk speaker John Naulty (aka `sather`) will host [a number of cryptography related sessions](https://events.ccc.de/congress/2024/hub/en/user/sather/) on topics adjacent to Milk Sad. We're interested in your questions and comments on our work!

<div id="toc-container" markdown="1">
<h2 class="no_toc">Table of Contents</h2>
* placeholder
{:toc}
</div>

<br/>

## Talk Notes
### Intro
If you're here, you probably saw our presentation (or a copy of the slides) and followed the QR code link here. Welcome!

We designed our presentation to be more approachable to viewers who do not interact with cryptography, cryptocurrencies, or security research on a daily basis. However, some of you do, or simply want to know more! Here are the more technical notes and references with links to support the presentation and show you some paths deeper down the rabbit hole.

### The Team
Our research has been the work of many people throughout different stages. The [credits section]({% link disclosure.md %}#credits) is the public list of people involved in CVE-2023-39910 and the initial intensive weeks of research and disclosure. To summarize, many friends and colleagues with a background in development and infosec got together to work out the initial mystery and its implications.

From there, a smaller subgroup continued the research into wallet details, real world usage and related vulnerabilities.

### The July 2023 Theft

The best place to start on this topic is our initial [disclosure writeup]({% link disclosure.md %}), which tells the initial story and context of wallet security, how we got involved, and how the initial events unfolded from our perspective.
If you want to jump ahead to the code snippets that explain how `bx seed` was vulnerable, go [here]({% link disclosure.md %}#our-cryptocurrency-is-gone-but-how).
If you would like to see the security research and disclosure from Ledger Donjon that preceded ours in early 2023 on a very similar vulnerability in `Trust Wallet`, go [here]({% link disclosure.md %}#not-even-the-second-hack-mersenne-twister-use-in-trust-wallet
).

The [disclosure timeline]({% link disclosure.md %}#basic-timeline-of-thefts-and-our-disclosure
) and our notes on the [libbitcoin team's response]({% link disclosure.md %}#basic-timeline-of-thefts-and-our-disclosure
) give some context on why the disclosure was extra difficult for us, in addition to the inherent complexities of quickly disclosing a serious vulnerability that is exploited in the wild.

Instead of trying to update the initial writeup article with all the new information, we switched to a different and more incremental format. On the [research updates](/updates.html) page, you find a list of separate articles on our progress and different topics.

For example, [update #1]({% link _posts/2023-11-22-research-update-1.md %}) from November 2023 shows details on the key sizes, address formats and path usage of the discovered wallets in the weak range associated with `libbitcoin-explorer`. There, we also describe weak wallets in that range which don't use the `BIP39` standard, and outline how much Ethereum we think was stolen in the July 2023 attack.
In [update #5]({% link _posts/2024-01-23-research-update-5.md %}#involvement-in-the-2023-07-12-theft), we described some additional data on how many Bitcoins were stolen in July 2023, and where they came from. 

### Technical Background

Cryptography and Cryptocurrencies are really complex subjects. In the presentation, we had to cut this down to the essentials.

Specifications:
* [BIP39 mnemonic](https://github.com/bitcoin/bips/blob/master/bip-0039.mediawiki) to store sensitive data in a human-readable format, and the initial key derivation
* [BIP32 hierarchical wallet](https://github.com/bitcoin/bips/blob/master/bip-0032.mediawiki) and child key derivation
* [BIP44](https://github.com/bitcoin/bips/blob/master/bip-0044.mediawiki), defines a format for typical derivation paths

Random number generators are also a big topic, so we'll just link to some of the bad ones that were involved in the flaws:
* [Mersenne Twister](https://en.wikipedia.org/wiki/Mersenne_Twister)
* [Linear congruential generator](https://en.wikipedia.org/wiki/Linear_congruential_generator), [MINSTD](https://en.wikipedia.org/wiki/MINSTD)

### The Milk Sad Project Name

Running `bx seed` with a system timestamp of `0` (PRNG seed `0x0`) to generate a 24-word always gives the following BIP39 mnemonic secret: 

```
milk sad wage cup reward umbrella raven visa give list decorate bulb gold raise twenty fly manual stand float super gentle climb fold park
```

### Brute Force Attack

In the absence of other shortcuts, trying all possible combinations is one of the most basic attacks and basically applies to all security systems out there which use secrets.

Modern symmetric key cryptography uses key sizes of over 112 bit, and the NIST recommendation for usage beyond 2030 is 128 bit. Through this, they benefit from the exponentially increasing complexity of naive brute-force attacks. Key sizes are different for some asymmetric key algorithms, so it's easier to think about AES than about RSA here. Correctly generated keys of such sizes (without other severe flaws) are thoroughly infeasible to brute-force, even with extreme amounts of computation power or special hardware and decades of patience.

By weakening cryptocurrency wallets from their supposed key strength of between 128 bit to 256 bit (depending on the chosen BIP39 variant) down to 32 bit, they turn from "unbreakable" to "everyone knows they're trivial to break", cryptographically speaking.

When researching weak wallets, the main challenge and computational effort is not in generating the weak initial PRNG data, but the BIP39 key derivation (SHA512 hashing) and the BIP32 key derivations with multiple child keys, which vary based on the evaluated derivation path. Additionally, finding wallets with actual usage on the blockchain requires pre-computing, storing and quickly checking information on wallet addresses - per blockchain (coin type)! Compared to this, generating relevant addresses from public keys is usually fairly cheap.

Some context on the practical aspects are in the [initial writeup]({% link disclosure.md %}#searching-for-wallets---implementation
). Our [update #3]({% link _posts/2023-12-06-research-update-3.md %}) goes into some depth on our data sources and the bloom filter we used for fast space-efficient lookups of addresses in memory. In [update #8]({% link _posts/2024-04-29-research-update-8.md %}), we further described the necessary work and how we were able to speed up the BIP32 computations by **6x** on our machine via changes to some core libraries via unorthodox changes (our code forks for this are public).

### Trust Wallet Flaws

The Trust Wallet software has the unfortunate distinction of having two separate, serious PRNG flaws over the lifetime of the software.

The "first" flaw, and the one we were aware of during our initial disclosure, is CVE-2023-31290. Here, a Trust Wallet variant also used the Mersenne Twister PRNG, and generated very similar (but different!) weak wallet ranges to `libbitcoin`. We covered this in the [initial writeup]({% link disclosure.md %}#not-even-the-second-hack-mersenne-twister-use-in-trust-wallet
) and [update #2]({% link _posts/2023-12-06-research-update-2.md %}).

In early 2024, the nice folks from `SECBIT Labs` disclosed CVE-2024-23660 on an older vulnerability, which we investigated in [update #5]({% link _posts/2024-01-23-research-update-5.md %}). In this "second" flaw, incorrect usage of the `trezor-crypto` library led to the use of the `MINSTD_RAND0` algorithm with `LCG16807` and time-based seeding when used under iOS. This is even easier to break then the other wallet ranges, since the key complexity is effectively less than `31` bit in this case.
In the same article, we also showed when relevant wallets were generated and how they were used.

### Wallets with Extreme History

That's a wild one, we know. See [update #7]({% link _posts/2024-04-20-research-update-7.md %}) for details.

### Cake Wallet

The Cake Wallet vulnerability has been public for years, and so we mentioned it in the [initial writeup]({% link disclosure.md %}#not-the-first-hack-weak-entropy-in-cake-wallet
). However, after some persistent digging and some luck, we were able to go further in the analysis and wallet recovery efforts than other people before us, leading to new results and a lengthy road to publication due to concerns about some remaining funds. 

See [update #6]({% link _posts/2024-02-13-research-update-6.md %}), [update #9]({% link _posts/2024-05-19-research-update-9.md %}), [update #10]({% link _posts/2024-10-12-research-update-10.md %}).

### Ethical Challenges

### Statistics

Details on the wallet numbers and usage statistics:
* extreme wallets: [update #7]({% link _posts/2024-04-20-research-update-7.md %})
* libbitcoin: [update #1]({% link _posts/2023-11-22-research-update-1.md %}) (combined BIP39, non-BIP39 wallets)
* Trust Wallet I: [update #2]({% link _posts/2023-12-06-research-update-2.md %})
* Trust Wallet IIa: [update #5]({% link _posts/2024-01-23-research-update-5.md %})
* Trust Wallet IIb: [update #5]({% link _posts/2024-01-23-research-update-5.md %})
* Cake Wallet: [update #10]({% link _posts/2024-10-12-research-update-10.md %})

### Defense

For wallet users, one particular mechanism we can recommend is to use a BIP39 passphrase. This is a separate secret from the BIP39 mnemonic phrase, and can be seen as an additional encryption layer. An attacker has to know both to derive the correct wallet keys and steal funds.
In the context of wallets generated from weak PRNGs, the use of a sufficiently complex BIP39 passphrase basically hides these keys from the attacker, giving the victim a lot more time and opportunities to learn about the problem and move their funds to a more secure wallet.

### Research Outlook

New exploratory wallet-related research is currently on hold. We still have some interesting things to share, but may take a while until we get around to writing them up.

### Data, Code

See [update #11]({% link _posts/2024-12-19-research-update-11.md %}).

If you want to get hands-on and investigate some of the discovered weak wallets on your own, the easiest place to start are our [published lists](https://git.distrust.co/milksad/data) of weak Bitcoin and Ethereum addresses. 

## Contact

See [contact]({% link index.md%}#contact).
---
title: /lookup
layout: home
permalink: /lookup.html
exclude: true
---

# Ride Closed 🚧

Dear users,

The Milk Sad vulnerability Lookup Service was offered for 90+ days by the Milk Sad team upon disclosure as a free service to help users determine if their wallets were impacted by the Milk Sad vulnerability. While there were significant cost considerations in running this service in the first place, we felt it was the right thing to do. Understandably, we cannot keep the service running forever.

This service is now offline.

[The code repository for the lookup service](https://git.distrust.co/milksad/lookup) will remain open and available to all for as long as we can reasonably do that.

Best regards,

Milk Sad team

-----
# _Original Page Content is Below:_
# (FOR HISTORICAL REFERENCE)

-----

# Lookup service

To help people identify if they are impacted by Milksad, we are providing a web service to check if your mnemonic is in the vulnerable set. Note that this service -only- covers mnemonics impacted by Libbitcoin Explorer (`bx`) versions `3.0.0` to `3.6.0`, though it may be updated over time to cover other related vulnerabilities we are researching.

## Who should use this service

* If you know you generated your wallet with bx 3.0.0 or higher (after ca. March 2017)
* If you know you generated your wallet with a CLI tool and don't remember which tool

## Security & Privacy

We of course do not want to store BIP39 mnemonics for this lookup service, or have people submit their BIP39 mnemonic private keys to us, so we had to sacrifice the overall user experience a bit in order to provide this service safely.

Our server contains `SHA-256` hashes of all currently known vulnerable mnemonics, and you can submit the `SHA-256 hash` of your own mnemonic and see if it is in our set.

Please note that it is usually a very, very bad idea to follow invitations from strangers on the Internet when it comes to sharing something about your wallet private keys. Typically they are scammers that have bad intentions. We're aware of this, and want to avoid being a bad example, hence we decided to include no convenient HTML input field that does the hashing for you (and could steal your mnemonics in the process). Users must bring their own sha256 hash of their mnemonic calculated on their own offline machine. If other people offer a similar lookup service, please be very cautious.

For those wishing to limit metadata sent to us or our server provider, we encourage using Whonix/Tor. That said, we do not have any logging enabled on this service.

## Usage

1. Obtain a hash of your mnemonic from an *offline* live-booted linux distribution running on hardware you trust

    Note: Do *not* substitute a virtual machine, WSL, your macbook, or anything else. Assume your workstation is compromised.
    ```
    unset HISTFILE
    set +o history
     printf 'milk sad wage cup reward umbrella raven visa give list decorate bulb gold raise twenty fly manual stand float super gentle climb fold park' | openssl sha256
    ```

2. Submit your hash to our comparison service on a separate -online- machine with network connection.

    Note: We suggest you transmit the hash to your online system by typing by hand, or copying it to an SD card.

    * Option 1: Browser

        Visit ~~[https://lookup.milksad.info](https://lookup.milksad.info)~~ (DEAD LINK) and paste your sha256 hash

    * Option 2: Terminal

        ```
        curl https://lookup.milksad.info/check?sha256=a7278e802055b3d17a66afe498428ac70c828ad69f25d02254c3b76b08d8cf8a
        ```

3. Inspect the response for ```vulnerable``` or ```no match```

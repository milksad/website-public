---
layout: post
title: "Update #5 - Digging Into New Trust Wallet Research"
author: ["Christian Reitter"]
date: 2024-01-23 17:00:00 +0000
---

Last Friday, we learned of a [newly disclosed vulnerability](https://secbit.io/blog/en/2024/01/19/trust-wallets-fomo3d-summer-vuln/) in the `Trust Wallet` software which is relevant to Milk Sad. Researchers from [SECBIT Labs](https://secbit.io) tracked down an older wallet generation weakness in the iOS platform version of `Trust Wallet` from 2018 and connected it to the large thefts on 2023-07-12 that triggered our Milk Sad research.<br/>
Using the newly available information, we managed to reproduce some of their findings, and can give a first look at additional data we collected.

<div id="toc-container" markdown="1">
<h2 class="no_toc">Table of Contents</h2>
* placeholder
{:toc}
</div>

<br/>

## Vulnerability TL;DR

Open source code shows that [a core component](https://github.com/trustwallet/trezor-crypto-ios/commits/master/) of the `Trust Wallet` app for iOS generated new cryptocurrency wallets using unsafe functions in the `trezor-crypto` library that were not meant for production. As a result, their wallet entropy is based on a weak PRNG with 31-bit initial state that is seeded with easy-to-guess time values. This makes all wallets generated with vulnerable app versions easy to brute force remotely, like the weak `bx seed` mechanism in Libbitcoin. Both weaknesses were attacked on-chain at the same time in July 2023.

If you want to understand this better, we recommend taking a look at the [Trust Wallet's Fomo3D Summer: Fresh Discovery of Low Entropy Flaw From 2018](https://secbit.io/blog/en/2024/01/19/trust-wallets-fomo3d-summer-vuln/) disclosure of the SECBIT team.

Credits for this discovery go to `p0n1` and `outoflegend` from SECBIT Labs. We weren't involved in their disclosure and didn't know about it beforehand, but they gave us some useful confirmations and hints via email after their disclosure was public. Thank you!

### Vulnerability Types

The newly disclosed vulnerability comes in two forms. Together with the previously known `Trust Wallet` vulnerability, things are getting confusing, so here's a short overview:

| Issue | When | Characteristics | Research | ID |
| -- | -- | -- | -- | -- | -- | -- | -- |
| vulnerable Trust Wallet browser extension | 2022/2023 | Mersenne Twister based | [research update #2]({% link _posts/2023-12-06-research-update-2.md %}) | CVE-2023-31290 |
| vulnerable Trust Wallet iOS app - **variant A** | Mid-2018 | `LCG16807` `MINSTD_RAND0`<br/> + `trezor-crypto` [2e528be](https://github.com/trezor/trezor-crypto/commit/2e528be1e91dd48c0e55061fbdd40ccf8b285559) | this article | CVE-2024-23660 |
| vulnerable Trust Wallet iOS app - **variant B** | Mid-2018 | `LCG16807` `MINSTD_RAND0`<br/> + `trezor-crypto` [009850](https://github.com/trezor/trezor-crypto/commit/009850f6c9afcf60b4c6280afd46868b1a7a1fdd) | this article | CVE-2024-23660 |

Each weak PRNGs variant creates different ranges of weak wallets. This article is _exclusively_ about the two newly discovered variants, and does not cover the Mersenne Twister based vulnerability.

## Early Results

Unlike other vulnerabilities with time-based PRNG seeding that we looked at before, this one calls the PRNG initialization in a way that can be predicted by outside observers, in both variant A and variant B:
```c
srand((unsigned)time(NULL));
```
`time(NULL)` returns the _seconds_ since 1970-01-01 00:00:00, representing the "random" base for `srand()` to pick the first PRNG state setting. Developers may know this time format as [Unix time](https://en.wikipedia.org/wiki/Unix_time).

For our initial experiments, we assume that the devices running the code had reasonably correct date and clock settings at the time they generated new wallets. The PRNG has a 31-bit state, but we used the known `srand()` behavior to focus our initial search onto a smaller subset of wallets which correspond to certain creation dates. This optimization speeds up the initial work.

### Over 6500 Weak Wallets 🔥
Within the wallet ranges with 12 mnemonic words (128 bits), we found the following:

| Range | Variant A | Variant B |
| creation timestamp in srand() | 2010-01-01 to <br/>2025-01-01 | 2010-01-01 to <br/>2025-01-01 |
| -- | -- | -- | -- | -- |
| Bitcoin P2PKH, compressed pubkey | 21 |  40 |
| Bitcoin P2SH-P2WPKH | 8 |  14 |
| Bitcoin P2WPKH | 452 | 216 |
| Ethereum | 4261 | 2054 |
| -- | -- | -- | -- | -- |
| total unique wallet private keys | 4455 | 2117 |

<details markdown=1>
<summary><b>Data details</b> (click to unfold)</summary>
* Wallet generation: variant A entropy -> BIP39 -> BIP32.
* Wallet generation: variant B entropy -> BIP39 -> BIP32.
* The 18 word and 24 word ranges in Variant A seem to be mostly unused, we focused on 12 word.
* Ethereum wallet detection is based on a known incomplete bloom filter data set from mid-2023.
* Bitcoin wallet detection is based on a bloom filter data set from 2024-01-15.
</details><br/>

Based on the current preliminary (incomplete!) data, we're counting at least **6572** unique wallet mnemonics, and we still expect to find more.

### Wallet Creation Date Pattern

We mapped out the discovered BTC and ETH wallets in a histogram plot to better understand the PRNG index range patterns (and therefore potential timestamps) the wallets in variant A and variant B were created with:

{% responsive_image_block %}
  figure: true
  path: assets/images/graphs/trustwallet2ab_bip39_128bit_only_histogram_btc_eth_prng_index_2010_2025_graph1.png
  alt: "Histogram of PRNG creation index for discovered wallets<br/>Trust Wallet iOS variant A & B - 128 bit wallets<br/>Yearly X-Axis timestamp markers, graph based on incomplete data"
  target_width: 1100px
{% endresponsive_image_block %}

<details markdown=1>
<summary><b>Graph details</b> (click to unfold)</summary>
* 2018-01-01: **1,514 x 10⁹** timestamp
* 2019-01-01: **1,546 x 10⁹** timestamp
* 2020-01-01: **1,578 x 10⁹** timestamp
* Total range shown: 2010-01-01 to 2025-01-01
* Y-Axis capped for readability, data is not stacked
* Variant B data only covers 2018-01-01 to 2020-01-01
* ETH data is outdated
</details><br/>

Two main observations:
1. The majority of Bitcoin and Ethereum wallets have a PRNG seed that corresponds to wallet creation timestamps from 2018 and 2019.
2. There is a smaller number of wallets with a more "random" pattern.

Point 1) generally fits to the publicly known timeline of the `Trust Wallet iOS` development and vulnerability (but is not a hard confirmation).
Point 2) indicates that there is some other source of weak wallets in this range that is less time based. Alternatively, it's possible that a subset of app devices had bad clocks for some reasons when creating the wallets.

Since the situation in 2018-2019 is very busy, here is a zoomed-in and annotated version:

{% responsive_image_block %}
  figure: true
  path: assets/images/graphs/trustwallet2ab_bip39_128bit_only_histogram_btc_eth_prng_index_2018_2019_graph2.png
  alt: "Histogram of PRNG creation index for discovered wallets - Trust Wallet iOS<br/>variant A & B, 2018-05 to 2020-01, data is not stacked, graph based on incomplete data"
  target_width: 1100px
{% endresponsive_image_block %}

A possible interpretation of the graph history:
1. Parameter space "before" the vulnerable `Trust Wallet` versions
2. Weak wallets of variant A appear with the first weak software version
3. Weak wallets of variant B appear once a differently vulnerable software version gets released
  * -> New wallets in variant A get more rare
4. A patched version get released
  * -> New wallets in variant B get more rare
5. Historically high BTC prices motivate users to create new wallet mnemonics for Bitcoin accounts (?)
6. Some users of both vulnerable software versions keep creating weak wallets months later, likely because they did not get or install the patched app versions yet

It is unclear to us if `Trust Wallet` on iOS had full support for Bitcoin wallets at the time, which may would be a relevant factor in the interpretation, and a clue towards the involvement of other wallet software.

### Bitcoin On-Chain Movements
Here is an _initial, incomplete_ analysis of BTC movements to and from weak wallets based on our initial data:
{% responsive_image_block %}
  figure: true
  path: assets/images/graphs/trustwallet2a_bip39_128bit_only_monthly_volume_btc_2018_2023_graph1.png
  alt: "Historic aggregated usage of known wallets in 128 bit Trust Wallet Bitcoin range - <b>variant A</b>"
  target_width: 950px
{% endresponsive_image_block %}

Notably, there is a visible peak in the graph of outgoing transfers of the month of 2023-07, which is no coincidence. More on this later.<br/><br/>

{% responsive_image_block %}
  figure: true
  path: assets/images/graphs/trustwallet2b_bip39_128bit_only_monthly_volume_btc_2018_2023_data2018_2019_graph1.png
  alt: "Historic aggregated usage of known wallets in 128 bit Trust Wallet Bitcoin range - <b>variant B</b>"
  target_width: 950px
{% endresponsive_image_block %}

Early estimations for on-chain funds movement volume:

| Variant A |  ca. **389,5 BTC**  | 2010-01-01 to 2025-01-01 |
| Variant B |  ca. **377,2 BTC**  | 2018-01-01 to 2020-01-01 |

Note: this describes funds moved over the weak wallets that have been at risk at some point, not the amounts stolen by attackers.

### Involvement in the 2023-07-12 Theft
Here are the three biggest outgoing theft transactions from the newly disclosed range of weak wallets on 2023-07-12:

| Transaction | Volume variant A<br/> (`Trust Wallet iOS`) | Volume variant B<br/> (`Trust Wallet iOS`) | Transaction total volume | Date |
| - | - | - | - |
| {{ "81cfe97cc16a49398d6986032ec8f6970ea80df5aa0990dcf0164de87136f5bf" | BtcLinkTxUrlSliced }} |		-4,829 BTC | -3,481 BTC |  -9,744 BTC | 2023-07-12 12:41 |
| {{ "cdd9a2aff7cd0707e31023513cc78aceff7ea7e754e3a9bde9c0482b70a9716c" | BtcLinkTxUrlSliced }} |		-8,161 BTC | none | -8,161 BTC _(incl. fee)_ | 2023-07-12 12:41 |
| {{ "a22b33a9a4ca0de2f56ef166298c186c5d71e56b944a255c2ecc52748f8f774b" | BtcLinkTxUrlSliced }} |		-9,523 BTC | -0,517 BTC | -14,846 BTC | 2023-07-12 12:41 |

As you can see, the newly discovered weak wallets make up a significant portion of some of the theft transactions on that day.
At least one of the transactions ({{ "cdd9a2aff7cd0707e31023513cc78aceff7ea7e754e3a9bde9c0482b70a9716c" | BtcLinkTxUrlSliced }}) had not been on our radar so far, since the stolen funds were exclusively coming from previously untracked wallets. We therefore have to adjust our estimates of the overall figure of BTC stolen from victims that day. It now exceeds $1M USD of equivalent value at the time!

### Current Status & Advice

At the time of publication, less than $100 USD in total remain on the known BTC wallets in the analyzed range subsets. Unfortunately, there have still been a number of incoming BTC transactions after 2023-07-12, which are very much at risk of getting stolen the second they get announced on the blockchain.

`Trust Wallet` users who created their mnemonic seed words with an app version downloaded during the time in question should be very careful. If in doubt, move funds to a new wallet mnemonic generated on a well-regarded and up-to-date wallet solution, such as a popular hardware wallet.

## Summary & Outlook

In this research update, we've followed up on work by other security researchers who discovered two large new ranges of weak cryptocurrency wallets from (most likely) an older vulnerability in `Trust Wallet` on iOS. We confirmed that these weak wallets exist and that they were involved in the Milk Sad thefts. We also provided new statistics on their distribution and usage.

This is an exciting new piece of the overall Milk Sad puzzle, and we'll continue to update this blog post with new information as it becomes available.

<br/>
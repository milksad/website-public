---
layout: post
title: "Update #11 - 38C3 Talk, Address Data, Early Code"
author: ["Christian Reitter"]
date: 2024-12-19 17:00:00 +0000
---

We will soon present a Milk Sad talk at the 38C3 conference, recently released large collections of discovered weak wallet addresses, and published more of our early code.

<div id="toc-container" markdown="1">
<h2 class="no_toc">Table of Contents</h2>
* placeholder
{:toc}
</div>

<br/>


## Milk Sad Talk at 38C3

We will present our work in [a talk](https://fahrplan.events.ccc.de/congress/2024/fahrplan/talk/PEN9QU/) at the 38th Chaos Communication Congress (38C3)!
38C3 happens in a few days in Hamburg, Germany, and multiple Milk Sad team members will be there. 

If you're at 38C3 and interested in our work, get in touch via [team@milksad.info](mailto:team@milksad.info) to meet up. You can also try your luck and ask for us at the [Church of Cryptography](https://events.ccc.de/congress/2024/hub/en/assembly/CoC/) assembly, where our team member and talk speaker John Naulty (aka `sather`) will host [a number of cryptography related sessions](https://events.ccc.de/congress/2024/hub/en/user/sather/) on topics adjacent to Milk Sad. We also invite you to join our extended Milk Sad [Q&A workshop session](https://events.ccc.de/congress/2024/hub/en/event/qa-dude-wheres-my-crypto/) right after the talk on the last day of Congress if you don't run into us before (no registration required).

For those of you not able to attend in person, the talk will be available via livestream and permanent video recording. We also plan to put our talk slides online. You'll find more information on our dedicated [38C3 talk page]({% link 38c3.md %}) on this website which will also host the talk notes once they're ready.

## New Research Data

We released a large collection of discovered Bitcoin and Ethereum addresses of weak wallets, see the new [data](https://git.distrust.co/milksad/data) repository.

They cover all five major PRNG ranges we looked at, and are split into different sub-ranges according to the basic key derivation approach, key sizes or other criteria. The currently released data sets consist of pooled lists of addresses with confirmed use on the blockchains, without specific references to the exact wallets they belong to. This format is a deliberate trade-off to allow research on the overall usage of individual weak ranges as well as helping to warn individual affected victims, without directly revealing all relevant information to attackers who didn't compute more specific details themselves.

We hope that releasing lists of weak addresses gives interested researchers more opportunities to look into the complex and fascinating on-chain behavior of the different weak wallet ranges without having to discover or handle the weak wallet private keys themselves. Combined with suitable software such Electrum in watch-only wallet mode, they allow following and graphing overall usage trends, calculating transaction volume, identifying suspicious patterns of potential attacks, and observing the the unfortunate practical effects of weak keys on newly incoming transactions into weak wallets. If you have specific notes on this data or do something cool with it, let us know at [team@milksad.info](mailto:team@milksad.info). Knowing that this data gets used may also motivate us more to do future updates or release more specific data sets.

Another new item in the data repository is a list of over 8700 hashes to identify mnemonics from vulnerable Cake Wallet users. This data has been public for over a year to allow client-side checks of weak wallet presence in patched Cake Wallet app versions. We now uploaded and documented this data in a more central location. 

## Early Research Code

We also open-sourced more of the early [research code](https://git.distrust.co/milksad/code). We wrote this code back in July 2023 during our dash towards CVE-2023-39910, and it represents our first days of interacting with the subject of `bx seed` behavior, weak BIP39 mnemonics, derived keys and initial Bitcoin address checks. While experimental and not ready-to-use, this may still be useful for some other researchers in the future, and gives some credits to the team members who were involved in the early development and security research work. 

Due to ethical concerns by the primary code author, the more advanced research code will not be completely public for the foreseeable future, but we'll revisit this again at a later time.

The newly published code comes in addition to the already-public [lookup](https://git.distrust.co/milksad/lookup) service code, the newly updated [Rust bloom filter generator](https://git.distrust.co/milksad/rust-bloom-filter-generator) code, and other relevant Rust libraries for [Rust secp256k1](https://git.distrust.co/milksad/rust-secp256k1-unsafe-fast) and [Rust bitcoin](https://git.distrust.co/milksad/rust-bitcoin-unsafe-fast) where we've made our optimized code variants publicly available.
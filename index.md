---
title: /summary
layout: home
permalink: /index.html
---

<img src="/assets/base/milksad_transparent.svg" width="30%" style="float: right"/>

# Milk Sad Disclosure

A practical explanation of how weak entropy can ruin your day - and your savings.

## Vulnerability [CVE-2023-39910](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-39910)

- We discovered a cryptographic weakness in the widely utilized `Libbitcoin Explorer` (`bx`) cryptocurrency wallet tool while following up on mysterious wallet thefts.
- The `bx seed` subcommand for generation of new wallet private key entropy is flawed and produces insecure output.
- On `Libbitcoin Explorer` `3.x` versions, `bx seed` uses the [Mersenne Twister](https://en.wikipedia.org/wiki/Mersenne_Twister#Disadvantages) pseudorandom number generator (PRNG) [initialized](https://github.com/libbitcoin/libbitcoin-system/blob/a1b777fc51d9c04e0c7a1dec5cc746b82a6afe64/src/crypto/pseudo_random.cpp#L77) with 32 bits of system time.
- Bad actors have discovered this flaw and are actively exploiting it to steal funds from affected wallets on multiple blockchains.
- We have reasons to believe some `Libbitcoin Explorer` versions before `3.0.0` also produce weak `bx seed` output in some system environments.
- Think of this as securing your online bank account with a password manager that creates a long random password, but it often creates the same passwords for every user. Malicious people have figured this out and drained funds on any account they can find.

### How?
<div style="float: left; margin-right:40px;margin-left:20px; margin-top:20px;margin-bottom:30px; width: 50%; display:table;">
    <div style="display:table-row; justify-content: space-around;">
      <img style="display: table-cell; vertical-align: middle;" width="25%" src="/assets/base/bx-mastering-bitcoin_left.jpg" />
      <img style="display: table-cell; vertical-align: middle;" width="75%" src="/assets/base/bx-mastering-bitcoin_right.jpg" />
    </div>
    <div style="display: table-footer;" style="font-size: 12px; margin-top: 10px;">
        Mastering Bitcoin - Second Edition by Andreas M. Antonopoulos LLC is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License.
    </div>
</div>
- Popular documentation like "[Mastering Bitcoin](https://bitcoinbook.info)" suggests the usage of `bx seed` for wallet generation.
- Secure cryptography requires a source of large, non-guessable numbers. If the random number generator is weak, the resulting cryptographic usage is almost always compromised.
- For technical people: in this case, practical wallet security is reduced from 128 bit, 192 bit or 256 bit to a mere 32 bit of unknown key information.
- A 32 bit key space is 2^32, or 4,294,967,296 different unique combinations of derived [BIP39](https://en.bitcoin.it/wiki/BIP_0039) mnemonic phrases or other key formats ([BIP32](https://en.bitcoin.it/wiki/BIP_0032)). Spoiler: That's not as many combinations as it sounds.
- With enough optimizations, a decent gaming PC can do a brute-force search through 2^32 wallet combinations in less than a day.
- Since `bx` has a configurable output length and can be used in several ways, there are a few variations the attacker needs to test for each case. This slows down practical attacks to a few days.
- Once an attacker finds a match of a wallet candidate with an actual wallet used on a blockchain, they are in full possession of the private keys and can steal remaining funds, trace all previous wallet history and sign messages.
- The attack works independent of the owner's current copy of the wallet secrets. In other words, even if you keep your paper wallet in a bank safe, your funds can still be stolen remotely. Crazy, right?
- Attackers are actively exploiting this and have been draining funds of wallets where the mnemonic was generated using this tool.
- Why the silly "Milk Sad" name? Running `bx seed` on `3.x` versions with a system time of 0.0 always generates the following secret:
  > milk sad wage cup reward umbrella raven visa give list decorate bulb gold raise twenty fly manual stand float super gentle climb fold park

### When?
The main theft occurred around 12 July 2023, although initial exploitation likely began at a smaller scale in May 2023.

A separate but similar vulnerability in another wallet software was detected in November 2022 and actively exploited shortly after, which may be the prequel to this story.

### Who?
We did not identify who is behind the ongoing thefts from vulnerable wallets.

### Type

[CWE-338: Use of Cryptographically Weak Pseudo-Random Number Generator (PRNG)](https://cwe.mitre.org/data/definitions/338.html)

### Vendor

[Libbitcoin](https://github.com/libbitcoin)

### Further Reading

See our [technical writeup](/disclosure.html), [research updates](/updates.html) and [FAQ](/faq.html).


### Related Vulnerabilities

Trust Wallet:
- [CVE-2023-31290](https://nvd.nist.gov/vuln/detail/CVE-2023-31290) - [vendor advisory](
https://community.trustwallet.com/t/browser-extension-wasm-vulnerability-postmortem/750787
) and [Ledger Donjon's technical writeup](https://blog.ledger.com/Funds-of-every-wallet-created-with-the-Trust-Wallet-browser-extension-could-have-been-stolen/
)
- [CVE-2024-23660](https://nvd.nist.gov/vuln/detail/CVE-2024-23660) - SECBIT Labs' [technical disclosure writeup](https://secbit.io/blog/en/2024/01/19/trust-wallets-fomo3d-summer-vuln/)

## Contact

* General requests and comments: email [team@milksad.info](mailto:team@milksad.info)
* Press: email [press@milksad.info](mailto:press@milksad.info)

## Team & Credits

{% include credits.html %}

# use Ruby as the foundation since Jekyll needs it
# Alpine to keep the container a bit lighter
FROM ruby:3-alpine AS builder
LABEL stage=milksad-website-builder

# install necessary system dependencies for Jekyll and plugins
RUN apk update && \
    apk add g++ make imagemagick imagemagick-dev imagemagick-libs && \
    rm -rf /var/cache/apk/*

RUN mkdir -p /home
# also creates user directory
RUN adduser -D builder
USER builder
RUN mkdir -p /home/builder/workdir
WORKDIR /home/builder/workdir

# copy the Jekyll website contents into the container
# this includes Gemfile and Gemfile.lock
# reminder: .dockerignore should specify a good ignorelist for this
COPY --chown=builder . .

# install Gems
RUN bundle install

# do the initial website build
RUN jekyll build

# note: this Dockerfile is no longer responsible for serving the content to end users

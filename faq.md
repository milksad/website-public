---
title: /faq
layout: home
permalink: /faq.html
---

# FAQ

### Why the name 'Milk Sad'?
`milk sad` were the first two words from the first seed phrase of this broken key generation process.

---
### Why this page?
This page is meant to increase awareness of this type of vulnerability, involving weak cryptographic systems due to inadequate sources of randomness, and help users protect their funds. Weak randomness is something that's easy to get wrong, and is unfortunately so common that one can frequently find software engineering books teaching this topic incorrectly. We hope this example can motivate developers and users to be more vigilant when dealing with generating sensitive cryptographic material.

---

### I had my cryptocurrency assets stolen and I believe it may be due to this vulnerability. What should I do?
Contact your local authorities. Look into how you generated your wallet in the first place, and if `bx` was involved at all.

You can use the [lookup service](/lookup.html) we developed to check if your wallet is impacted by this specific problem.

**We do not have your funds.**

---

### Has law enforcement been notified?
Yes. If you're in the US, American Federal Agents have been notified of the issue and the extent of the currently known impact. Further disseminating this information to your country's authorities may prove helpful.

---

### Have there been similar vulnerabilities in the past?
Yes, this issue is very similar to Trust Wallet, CVE-2023-31290, see the [blog post from Ledger Donjon](https://blog.ledger.com/Funds-of-every-wallet-created-with-the-Trust-Wallet-browser-extension-could-have-been-stolen/). Other related vulnerabilities are listed in the [write-up](/disclosure.html).

---

### I created my wallet with `bx seed`, but my funds are still there -- which versions were vulnerable?
All versions of `bx` `3.0.0` to `3.6.0` have weak entropy generation. Additionally, we have [some indication](/disclosure.html#bx-other-rng-issues) that the initial `sx newseed` and some `bx` `2.x` versions behave insecurely on some systems. We recommend quickly moving all funds on wallets created with `bx seed` regardless of the original version and assuming the worst.

---

### I used `bx` to generate my wallets but only use it for non-BTC coins, do I need to worry?
Yes. All funds stored on BIP39 mnemonic secrets or BIP32 wallet seeds are affected since the underlying private keys are basically public now.

---

### I used `bx` to handle some wallet secrets, but generated them elsewhere, do I need to worry?
The identified problem is limited to the entropy generation functionality. We're not aware of serious issues in other `bx` commands that operate on existing wallets. If you are 100% sure you generated the wallet elsewhere, you should be okay.

---

### Is my hardware wallet affected?
  - If you _generated_ your BIP39 mnemonic secret on a hardware wallet, you should be fine.
  - We are not aware of any hardware wallet with similarly broken _wallet generation_ - Trezor, Ledger, and other hardware wallets do not use the affected Libbitcoin code.
  - If you _imported_ a vulnerable wallet into other software wallets or hardware wallets, they cannot protect you.

---

### Is the vulnerability currently fixed in `libbitcoin-explorer`?
We are not aware of a fix. At the time of disclosure, our understanding is that the Libbitcoin team considers this not to be a vulnerability. See [this section](/disclosure.html#libbitcoin-vendor-response) in our disclosure.

**Update**: `libbitcoin-explorer` [3.8.0](https://github.com/libbitcoin/libbitcoin-explorer/releases/tag/v3.8.0) fixed the issue by removing the problematic entropy generation command.

---

### Would a security patch fix this problem?
Only for new wallets. All funds stored using previously generated wallets will remain vulnerable, and need to be migrated to a new secure wallet.

---

### How do the researchers involved in this project personally protect their crypto assets?
The team members use a wide array of commercially available hardware wallets as well as custom built solutions, while some do not have crypto-assets at all.

---

### In a nutshell, what are your personal "lessons learned"?
See [lessons learned](/disclosure.html#lessons-learned).

---

### Is my major exchange cold storage impacted?
If it doesn't rely on `bx seed`, then we have no indications for it.

---


### Do you have any general advice for avoiding this type of flaw?
   - If you are non-technical, don't try to store large amounts of crypto assets yourself without consulting security experts.
   - If you are highly technical and lazy or not well informed on security topics, don't try to store large amounts of crypto assets yourself.
   - Don't trust closed source solutions, or open source solutions no one credible has reviewed recently.
   - Use a hardware wallet, but add a [passphrase](https://trezor.io/learn/a/passphrases-and-hidden-wallets) for good measure.

---

### Any recommendations for experts?

We could fit an entire book of advice here, but some top of mind items:

- Generate keys you care about offline:
  - Distrust provides [AirgapOS](https://git.distrust.co/public/airgap) for offline operations.
- Use a BIP39 passphrase with 128 bit entropy or higher, generated completely independently from your mnemonic.
- Ensure your key is actually random:
  - Ensure /dev/urandom has multiple hardware sources of entropy (Consider using a [TRNG](https://github.com/waywardgeek/infnoise) or similar hardware).
  - Ensure mnemonic is -actually- seeded with 128-256 bits from /dev/urandom.
  - Consider generating a mnemonic with [dice](https://github.com/taelfrinn/Bip39-diceware), or a [TRNG](https://github.com/waywardgeek/infnoise).
- Derive addresses with two independent platforms:
  - E.g. hardware wallet + airgapped CLI tools should agree on addresses.
- Review the entire code path to your mnemonic/passphrase:
  - Be aware of the kernel version and `random.c` algorithm you are using.
- Document every tool and line of code in your key generation path.

---

### Do you accept donations?
No.

---

### Can I hire any of you?

Sure! If you like our work, check out the following commercial services offered by different team members and organizations.

{% include hiring.html %}
